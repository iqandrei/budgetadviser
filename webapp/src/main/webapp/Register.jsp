<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="language" value="${pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="jspinternational/text"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="${language}">

<body>

	<form action="Controller" method="post">

		<label for="username"><fmt:message
				key="register.label.username" />:</label> <input type="text" id="username"
			name="username" /><br> <label for="password"><fmt:message
				key="register.label.password" />:</label> <input type="text" id="password"
			name="password" /><br> <label for="email"><fmt:message
				key="register.label.email" />:</label> <input type="text" id="email"
			name="email" /><br> <label for="firstname"><fmt:message
				key="register.label.firstname" />:</label> <input type="text"
			id="firstname" name="firstname" /><br> <label for="lastname"><fmt:message
				key="register.label.lastname" />:</label> <input type="text" id="lastname"
			name="lastname" /><br>



		<fmt:message var="buttonValue" key="register.button.submit" />
		<input type="hidden" name="action" value="REGISTER"> <input
			type="submit" value="${buttonValue}" />
	</form>

	<form action="Login.jsp" method="post">
		<fmt:message var="buttonValue" key="register.button.back" />
		<input Type="submit" value="${buttonValue}">
	</form>
</body>
</html>