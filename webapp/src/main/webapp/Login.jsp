<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="language"
	value="${pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="jspinternational/text" />
<!DOCTYPE html>
<html lang="${language}">

<body>

	<form method="post" action="Controller" method="post">
		<label for="username"><fmt:message key="login.label.username" />:</label>
		<input type="text" id="username" name="username"> <br> <label
			for="password"><fmt:message key="login.label.password" />:</label> <input
			type="password" id="password" name="password"> <br>

		<fmt:message var="buttonValue" key="login.button.submit" />
		<input type="hidden" name="action" value="Login"> <input
			type="submit" name="submit" value="${buttonValue}">
	</form>

	<a href="Register.jsp"><fmt:message key="login.label.unregistered" /></a>
</body>
</html>
