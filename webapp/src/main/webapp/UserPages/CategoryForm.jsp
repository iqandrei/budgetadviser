<%@include file="Tasktoolbar.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Category</title>
</head>
<body>
	<form action="Controller" method="post">
	
		<label for="name"><fmt:message key="categoryform.label.name" />:</label>
		<input id="name" type="text" name="name" /><br /> 
		
		<label for="description"><fmt:message key="categoryform.label.description" />:</label> 
		<input id="description" type="text" name="description" /><br /> 
			
			<select name="parentCategory">
			<option value="none">
			<c:forEach var="category" items="${requestScope.categories}">
				<option value="${category.getId()}">${category.getName()}
			</c:forEach>
			
		</select> <input type="hidden" name="action" value="addCategory">
		<fmt:message var="buttonValue" key="categoryform.transaction.submit" />
		<input type="submit" value="${buttonValue}" />
	</form>
	
</body>
</html>