<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="Tasktoolbar.jsp" %>

<c:set var="language"
	value="${pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="jspinternational/text" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang=${language}>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form action="Controller" method="post">
		<label for="note"><fmt:message key="transaction.label.note" />:</label>
			<input id="note" type="text" name="note" /><br /> 
		
			<label for="amount"><fmt:message key="transaction.label.amount" />:</label>
				<input id="amount"  type="text" name="amount" /><br />
						
			<label for="date"><fmt:message key="transaction.label.date" />:</label>
				<input id="date" type="text" name="date" /><br /> 
				
				
		 
		<input type="radio" name="type" value="INCOME" checked>
		<fmt:message key="transaction.radio.income" />
		
		<input type="radio" name="type" value="EXPENSE">
			<fmt:message key="transaction.radio.expense" />
			
		 <select name="category">
			<option value="none" />
				 <c:forEach var="category" items="${requestScope.categories}">
				 	<option value="${category.getId()}">${category.getName()}
				 </c:forEach>
			</select>
			
			 <select name="account">
				 <c:forEach var="account" items="${requestScope.accounts}">
				 	<option value="${account.getId()}">${account.getTitle()}
				 </c:forEach>
			</select>
			
		
		<input type="hidden" name="action" value="AddTransaction">
		<input type="hidden" name="accountId" value="${requestScope.accountId}">
		<fmt:message var="buttonValue" key="transaction.button.submit" />
		<input type="submit" value="${buttonValue}"/>
	</form>
		
		<form action="Controller" method="post">
			<input type="hidden" name="action" value="categoryForm">
			<fmt:message var="buttonValue" key="transaction.button.addCategory" />
			<input type="submit" value="${buttonValue}"/>
		</form>
				

</body>
</html>