<%@include file="Tasktoolbar.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Accounts</title>
</head>
<body>
	<c:choose>
		<c:when test="${!empty requestScope.accounts}">
			<c:forEach var="account" items="${requestScope.accounts}">
				<form action="Controller" method="post">
					<input type="hidden" name="action" value="viewAccountInfo">
					<input type="hidden" name="accountId" value="${account.getId()}">
					<input type="submit" value="${account.getTitle()}" /> <br />
				</form>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<fmt:message key="accounts.message.noaccount" />
		</c:otherwise>
	</c:choose>
	<form action="Contr	oller" method=post>
		<input type="hidden" name="action" value="accountForm">
		<fmt:message var="buttonValue" key="accounts.button.addAccount" />
		<input type="submit" value="${buttonValue}" />
	</form>
</body>
</html>