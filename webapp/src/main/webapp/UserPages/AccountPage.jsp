<%@include file="Tasktoolbar.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<font size=5><fmt:message key="account.text.title" /></font>: ${requestScope.account.getTitle()}
	<br>
	<font size=3><fmt:message key="account.text.balance" /></font>: ${requestScope.account.getBalance()}
	<br>
	<font size=3><fmt:message key="account.text.currency" /></font>: ${requestScope.account.getCurrencyCode().getCode()}
	<br>
	<c:choose>
		<c:when test="${!empty requestScope.account.getTransactions()}">
		<font size=5><fmt:message key="account.text.transactions"></fmt:message></font>
			<table  border="1" style="width:100%">
			    <tr>
       				<th>Name</th>
        			<th>Amount</th>
        			<th>Date</th>
       				<th>Type</th>
       				<th>Account</th>     				
       				<th>Category</th>
       				
    			</tr>
				<c:forEach var="transaction"
					items="${requestScope.account.getTransactions()}">
					<tr>
						<td>${transaction.getNote()}</td>  	
						<td>${transaction.getAmount()}</td>
						<td>${transaction.getDate()}</td>
						<td>${transaction.getType() }</td>
						<td>${transaction.getSource().getTitle()}</td>
						<td>${transaction.getCategory().getName()}</td>
					</tr>
				</c:forEach>
			</table>
		</c:when>
		<c:otherwise>
			<fmt:message key="account.text.notransaction" />
		</c:otherwise>
	</c:choose>
		
	<form action="Controller" method=post>
		<input type="hidden" name="accountId" value="${requestScope.account.getId()}">
		<input type="hidden" name="action" value="transactionForm">
		<fmt:message var="buttonValue" key="account.button.addTransaction" />
		<input type="submit" value="${buttonValue}" />
	</form>

</body>
</html>
