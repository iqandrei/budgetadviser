<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="language" value="${pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="jspinternational/text" />


<c:set var="language"
	value="${pageContext.request.locale}"
	scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="${language}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form  action="Controller" method="post">
	<input type="hidden" name="action" value="viewAccounts">
	<fmt:message var="accounts"  key="taskbar.button.accounts" />
	<input type="submit" value="${accounts}"/>
</form>


<form  action="Controller" method="post">
	<input type="hidden" name="action" value="Logout">
	<fmt:message var="logout" key="taskbar.button.logout" />
	<input type="submit" value="${logout}"/>
</form>
</body>
</html>