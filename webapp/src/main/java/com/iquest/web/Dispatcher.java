package com.iquest.web;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.iquest.web.controller.AccountController;
import com.iquest.web.controller.CategoryController;
import com.iquest.web.controller.SessionController;
import com.iquest.web.controller.TransactionController;
import com.iquest.web.controller.UserController;
import com.iquest.web.exception.CriticalException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class used to retrieve user actions and respond to them accordingly.
 * 
 * @author DawnBreak
 *
 */
public class Dispatcher extends HttpServlet {
	EntityManagerFactory entManagerFact;
	private final Logger logger = LoggerFactory.getLogger(Dispatcher.class);

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		entManagerFact = Persistence
				.createEntityManagerFactory("com.iquest.manager");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Locale userLocale = request.getLocale();
		ResourceBundle international = ResourceBundle.getBundle(
				ParameterConstants.INTERNATIONAL, userLocale);
		try {
			UserController userController;
			TransactionController transController;
			SessionController sessionController;
			AccountController accountController;
			CategoryController categoryController;

			EntityManager entManager = entManagerFact.createEntityManager();
			request.setAttribute(ParameterConstants.ENTITYMANAGER, entManager);

			RequestDispatcher userView;
			String userRedirectLink;

			String requestPath = request
					.getParameter(ParameterConstants.USERACTION);
			if (requestPath == null) {
			}

			switch (UserAction.valueOf(requestPath.toUpperCase())) {
			case LOGIN:
				sessionController = new SessionController();
				userRedirectLink = sessionController.fetchLoginDisplay(request,
						response);
				userView = request.getRequestDispatcher(userRedirectLink);
				userView.include(request, response);
				break;

			case REGISTER:
				userController = new UserController();
				userRedirectLink = userController.fetchRegisterResponse(
						request, response);
				userView = request.getRequestDispatcher(userRedirectLink);
				userView.include(request, response);
				break;

			case LOGOUT:
				sessionController = new SessionController();
				userRedirectLink = sessionController.fetchLogoutDisplay(
						request, response);
				response.sendRedirect(userRedirectLink);
				break;
			case VIEWACCOUNTS:
				accountController = new AccountController();
				userRedirectLink = accountController.fetchUserAccountsResponse(
						request, response);
				userView = request.getRequestDispatcher(userRedirectLink);
				userView.forward(request, response);
				break;
			case ADDACCOUNT:
				accountController = new AccountController();
				userRedirectLink = accountController.fetchAddAccountResponse(
						request, response);
				userView = request.getRequestDispatcher(userRedirectLink);
				userView.forward(request, response);
				break;
			case ACCOUNTFORM:
				accountController = new AccountController();
				userRedirectLink = accountController.fetchAccountCurrencies(
						request, response);

				userView = request.getRequestDispatcher(userRedirectLink);
				userView.forward(request, response);
				break;
			case VIEWACCOUNTINFO:
				accountController = new AccountController();
				userRedirectLink = accountController
						.fetchAccountResponse(request);
				userView = request.getRequestDispatcher(userRedirectLink);
				userView.forward(request, response);
				break;

			case TRANSACTIONFORM:
				transController = new TransactionController();
				userRedirectLink = transController
						.fetchTransactionFormResponse(request, response);
				userView = request.getRequestDispatcher(userRedirectLink);
				userView.forward(request, response);
				break;

			case ADDTRANSACTION:
				transController = new TransactionController();
				userRedirectLink = transController.fetchAddTransactionResponse(
						request, response);
				userView = request.getRequestDispatcher(userRedirectLink);

				userView.include(request, response);
				break;
			case VIEWTRANSACTIONS:
				transController = new TransactionController();
				break;

			case CATEGORYFORM:
				categoryController = new CategoryController();
				categoryController.fetchViewCategoriesResponse(request,
						response);
				userRedirectLink = "UserPages/CategoryForm.jsp";
				userView = request.getRequestDispatcher(userRedirectLink);
				userView.forward(request, response);
				break;

			case ADDCATEGORY:
				categoryController = new CategoryController();
				userRedirectLink = categoryController.fetchAddCategoryResponse(
						request, response);
				userView = request.getRequestDispatcher(userRedirectLink);
				userView.forward(request, response);
				break;
			}
		} catch (CriticalException sfe) {
			throw sfe;
		} catch (Throwable t) {
			t.printStackTrace();
			throw new CriticalException(
					international.getString("dispatcher.system.exception"));
		}

	}

	@Override
	public void destroy() {
		super.destroy();

		entManagerFact.close();
	}
}
