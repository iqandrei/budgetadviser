package com.iquest.web;

public abstract class ParameterConstants {
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String EMAIL = "email";
	public static final String FIRSTNAME = "firstname";
	public static final String LASTNAME = "firstname";
	public static final String USERACTION = "action";
	
	public static final String ENTITYMANAGER = "EntityManager";
	public static final String INTERNATIONAL = "/Internationalization/text";

}
