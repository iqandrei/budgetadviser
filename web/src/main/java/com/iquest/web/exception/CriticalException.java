package com.iquest.web.exception;

/**
 * Exception class which is thrown in case the user's request can not be
 * performed and the cause does not depend on the user.
 * 
 * @author andrei.georgescu
 *
 */
public class CriticalException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor which calls the super class constructor with a throwable
	 * cause.
	 * 
	 * @param cause
	 *            The throwable which is going to be passed to the super class
	 *            constructor.
	 */
	public CriticalException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructor which calls the super class constructor with a message.
	 * 
	 * @param message
	 *            The string which is going to be passed to the super class
	 *            constructor.
	 */
	public CriticalException(String message) {
		super(message);
	}

	/**
	 * Constructor which calls the super class constructor with a message and a
	 * cause.
	 * 
	 * @param message
	 *            The string which is going to be passed to the super class
	 *            constructor.
	 * @param cause
	 *            The cause which is going to be passed to the super class
	 *            constructor.
	 */
	public CriticalException(String message, Throwable cause) {
		super(message, cause);
	}
}
