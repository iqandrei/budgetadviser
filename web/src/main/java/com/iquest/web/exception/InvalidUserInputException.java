package com.iquest.web.exception;

/**
 * Exception class which is thrown in case the user input is invalid. 
 * @author andrei.georgescu
 *
 */
public class InvalidUserInputException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor which calls the super class constructor with a throwable
	 * cause.
	 * 
	 * @param cause
	 *            The throwable which is going to be passed to the super class
	 *            constructor.
	 */
	public InvalidUserInputException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructor which calls the super class constructor with a message.
	 * 
	 * @param message
	 *            The string which is going to be passed to the super class
	 *            constructor.
	 */
	public InvalidUserInputException(String message) {
		super(message);
	}

	/**
	 * Constructor which calls the super class constructor with a message and a
	 * cause.
	 * 
	 * @param message
	 *            The string which is going to be passed to the super class
	 *            constructor.
	 * @param cause
	 *            The cause which is going to be passed to the super class
	 *            constructor.
	 */
	public InvalidUserInputException(String message, Throwable cause) {
		super(message, cause);
	}
}
