package com.iquest.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;
import java.util.ResourceBundle;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iquest.persistence.entity.Account;
import com.iquest.persistence.entity.Currency;
import com.iquest.persistence.entity.Transaction;
import com.iquest.persistence.entity.User;
import com.iquest.service.account.AccountManagerImpl;
import com.iquest.service.exception.NoResultException;
import com.iquest.service.exception.ServiceException;
import com.iquest.service.transactionservice.TransactionManager;
import com.iquest.service.transactionservice.TransactionManagerImpl;
import com.iquest.web.ParameterConstants;
import com.iquest.web.SessionConstants;
import com.iquest.web.exception.InvalidUserInputException;
import com.iquest.web.exception.CriticalException;

/**
 * Class used to fulfill requests associated with operations on
 * accounts.
 * 
 * @author andrei.georgescu
 *
 */
public class AccountController {
	/**
	 * The logger of the class. It logs information regarding exceptions which
	 * are thrown from this class.
	 */
	private final Logger logger = LoggerFactory
			.getLogger(AccountController.class);

	/**
	 * Method used to add an account and return the next page that is going to
	 * be displayed to the user.
	 * 
	 * @param request
	 *            The request object which is sent from the user.
	 * @param response
	 *            The response object which is going to be sent to the user.
	 * @return A string which represents the path to the next page displayed to
	 *         the user.
	 * @throws CriticalException
	 *             Thrown if there is a problem and the method can not fulfill
	 *             the user's request.
	 */
	public String fetchAddAccountResponse(HttpServletRequest request,
			HttpServletResponse response) throws CriticalException {
		ResourceBundle languages = ResourceBundle.getBundle(
				ParameterConstants.INTERNATIONAL, request.getLocale());

		try {
			AccountManagerImpl accountManager = new AccountManagerImpl();

			EntityManager entManager = (EntityManager) request
					.getAttribute(ParameterConstants.ENTITYMANAGER);

			Account newAccount = extractAccountData(request);

			accountManager.addUserAccount(newAccount, entManager);
		} catch (InvalidUserInputException iuie) {
			try {
				setClientMessage(response, iuie.getMessage());
				return "UserPages/AccountForm.jsp";

			} catch (IOException ie) {
				throw new CriticalException("dispatcher.system.exception");
			}
		} catch (ServiceException se) {
			throw new CriticalException(
					languages.getString("dispatcher.system.exception"));
		}
		return "Controller?action=viewAccounts";
	}

	/**
	 * Method used to retrieve the accounts associated with a particular user.
	 * 
	 * @param request
	 *            The request object which is sent from the user.
	 * @param response
	 *            The response object which is going to be sent to the user.
	 * @return A string which represents the path to the next page displayed to
	 *         the user.
	 * @throws CriticalException
	 *             Thrown if there is a problem and the method can not fulfill
	 *             the user's request.
	 */
	public String fetchUserAccountsResponse(HttpServletRequest request,
			HttpServletResponse response) throws CriticalException {
		ResourceBundle languages = ResourceBundle.getBundle(
				ParameterConstants.INTERNATIONAL, request.getLocale());

		try {
			HttpSession userSession = request.getSession(false);
			User user = (User) userSession.getAttribute(SessionConstants.USER);

			EntityManager entManager = (EntityManager) request
					.getAttribute(ParameterConstants.ENTITYMANAGER);

			AccountManagerImpl accountManager = new AccountManagerImpl();
			List<Account> accounts = accountManager.retriveUserAccounts(user,
					entManager);
			request.setAttribute("accounts", accounts);

		} catch (NoResultException nre) {
			return "UserPages/AccountsPage.jsp";
		} catch (ServiceException se) {
			se.printStackTrace();
			throw new CriticalException(
					languages.getString("dispatcher.system.exception"));
		}
		return "UserPages/AccountsPage.jsp";

	}

	/**
	 * Method used to retrieve the available currencies for account creation.
	 * 
	 * @param request
	 *            The request object which is sent from the user.
	 * @param response
	 *            The response object which is going to be sent to the user.
	 * @return A string which represents the path to the next page displayed to
	 *         the user.
	 * @throws CriticalException
	 *             Thrown if there is a problem and the method can not fulfill
	 *             the user's request.
	 */
	public String fetchAccountCurrencies(HttpServletRequest request,
			HttpServletResponse response) throws CriticalException {

		EntityManager entManager = (EntityManager) request
				.getAttribute(ParameterConstants.ENTITYMANAGER);

		AccountManagerImpl accountManager = new AccountManagerImpl();
		List<Currency> currencies = accountManager
				.retrieveAvailableCurrencies(entManager);
		request.setAttribute("currencies", currencies);

		return "UserPages/AccountForm.jsp";
	}

	/**
	 * @param request
	 *            The request object which is sent from the user.
	 * @param response
	 *            The response object which is going to be sent to the user.
	 * @return A string which represents the path to the next page displayed to
	 *         the user.
	 * @throws CriticalException
	 *             Thrown if there is a problem and the method can not fulfill
	 *             the user's request.
	 */
	public String fetchAccountResponse(HttpServletRequest request) {
		ResourceBundle languages = ResourceBundle.getBundle(
				ParameterConstants.INTERNATIONAL, request.getLocale());
		try {
			EntityManager entManager = (EntityManager) request
					.getAttribute(ParameterConstants.ENTITYMANAGER);

			Long accountId = Long.parseLong(request.getParameter("accountId"));
			AccountManagerImpl accountManager = new AccountManagerImpl();

			Account userAccount = accountManager.retrieveAccount(accountId,
					entManager);

			request.setAttribute("account", userAccount);

			TransactionManager transactionManager = new TransactionManagerImpl();
			List<Transaction> accountTransactions = transactionManager
					.getAccountTransactions(userAccount, entManager);

			request.setAttribute("transactions", accountTransactions);

			return "UserPages/AccountPage.jsp";

		} catch (NoResultException nre) {
			logger.error("Entity is not in the database", nre);
			return "UserPages/AccountPage.jsp";
		} catch (ServiceException se) {
			logger.error("User can not be logged in", se);
			throw new CriticalException(
					languages.getString("dispatcher.system.exception"));
		}
	}

	/**
	 * Method used to extract build an account from the request data the user
	 * has provided.
	 * 
	 * @param request
	 *            The request object which is sent by the user and which
	 *            contains the data from which an account can be built.
	 * @return An account object.
	 * 
	 * @throws InvalidUserInputException
	 *             Thrown if the information that is sent from the user is not
	 *             valid.
	 */
	private Account extractAccountData(HttpServletRequest request)
			throws InvalidUserInputException {
		Account newAccount = new Account();

		HttpSession userSession = request.getSession(false);
		User user = (User) userSession.getAttribute(SessionConstants.USER);

		String title = request.getParameter("title");
		String startingAmmount = request.getParameter("balance");
		String currency = request.getParameter("currency");

		if (title == null) {
			throw new InvalidUserInputException(
					"Account title can not be null!");
		}
		newAccount.setTitle(title);

		if (startingAmmount != null && !startingAmmount.isEmpty()) {
			try {
				newAccount.setBalance(new BigDecimal(startingAmmount));

			} catch (NumberFormatException nfe) {
				throw new InvalidUserInputException(
						"Starting amount needs to be a number", nfe);
			}
		}
		Currency accountCurrency = new Currency();
		accountCurrency.setCode(currency);

		newAccount.setCurrencyCode(accountCurrency);
		newAccount.setUser(user);

		return newAccount;
	}

	/**
	 * Method used to set the message the user is going to see on the response.
	 * 
	 * @param response
	 *            The response object on which we are going to put the user
	 *            response message.
	 * @param message
	 *            The message to the user informing him if something has gone
	 *            sour.
	 * 
	 * @throws IOException
	 *             Thrown if we can not write the messages to the response.
	 *
	 */
	private void setClientMessage(HttpServletResponse response, String message)
			throws IOException {
		response.setContentType("text/html");
		PrintWriter responseMessage = response.getWriter();
		responseMessage.println(message);
	}
}
