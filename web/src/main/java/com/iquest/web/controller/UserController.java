package com.iquest.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ResourceBundle;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iquest.persistence.entity.User;
import com.iquest.service.clientservice.UserManager;
import com.iquest.service.clientservice.UserManagerImpl;
import com.iquest.service.exception.ServiceException;
import com.iquest.web.ParameterConstants;
import com.iquest.web.exception.CriticalException;

/**
 * Class used to fulfill requests associated with operations on users.
 * 
 * @author andrei.georgescu
 *
 */
public class UserController {
	/**
	 * The logger field which stores information about errors or how the class
	 * operates.
	 */
	private final Logger logger = LoggerFactory.getLogger(UserController.class);

	/**
	 * Method used to register the user and retrieve the next page he is going
	 * to see.
	 * 
	 * @param request
	 *            The request that is sent from the user.
	 * @param response
	 *            The response which is going to be sent to the user.
	 * @return The path to the next page that is going to be displayed to the
	 *         user.
	 * @throws CriticalException
	 *             Thrown if the system is incapable to manage the request.
	 */
	public String fetchRegisterResponse(HttpServletRequest request,
			HttpServletResponse response) throws CriticalException {
		ResourceBundle languages = ResourceBundle.getBundle(
				ParameterConstants.INTERNATIONAL, request.getLocale());

		try {
			EntityManager entManager = (EntityManager) request
					.getAttribute(ParameterConstants.ENTITYMANAGER);

			User userData = fetchUserData(request);

			UserManager userManager = new UserManagerImpl();

			boolean emailExists = userManager.checkEmailAvailability(
					userData.getEmail(), entManager);
			if (emailExists) {
				setClientMessage(response,
						languages.getString("user.register.emailfail"));
				return "Register.jsp";
			}

			boolean usernameExists = userManager.checkEmailAvailability(
					userData.getUsername(), entManager);
			if (usernameExists) {
				setClientMessage(response,
						languages.getString("user.register.usernamefail"));
				return "Register.jsp";
			}

			userManager.addUser(userData, entManager);
			setClientMessage(response,
					languages.getString("user.register.successful"));
			return "Login.jsp";

		} catch (IOException ioe) {
			logger.error("Can not write to response writer", ioe);
			throw new CriticalException(
					languages.getString("dispatcher.system.exception"));
		} catch (ServiceException se) {
			logger.error("Service can not fulfill request", se);

			throw new CriticalException(
					languages.getString("dispatcher.system.exception"));
		}
	}

	/**
	 * Method used to retrieve user data from the request object.
	 * 
	 * @param request
	 *            The request from which the information is being extracted.
	 * @return The user object which contains the information from the request.
	 */
	private User fetchUserData(HttpServletRequest request) {
		String username = request.getParameter(ParameterConstants.USERNAME);
		String password = request.getParameter(ParameterConstants.PASSWORD);
		String email = request.getParameter(ParameterConstants.EMAIL);
		String firstname = request.getParameter(ParameterConstants.FIRSTNAME);
		String lastname = request.getParameter(ParameterConstants.LASTNAME);

		User userData = new User();
		userData.setUsername(username);
		userData.setPassword(password);
		userData.setEmail(email);

		userData.setFirstname(firstname);
		userData.setLastname(lastname);

		return userData;
	}

	/**
	 * Method used to set the message the user is going to see on the response.
	 * 
	 * @param response
	 *            The response object on which we are going to put the user
	 *            response message.
	 * @param message
	 *            The message to the user informing him if something has gone
	 *            sour.
	 * 
	 * @throws IOException
	 *             Thrown if we can not write the messages to the response.
	 *
	 */
	private void setClientMessage(HttpServletResponse response, String message)
			throws IOException {
		response.setContentType("text/html");
		PrintWriter responseMessage = response.getWriter();
		responseMessage.println(message);
	}
}
