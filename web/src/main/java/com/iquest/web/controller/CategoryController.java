package com.iquest.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.ResourceBundle;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.iquest.persistence.entity.Category;
import com.iquest.persistence.entity.User;
import com.iquest.service.clientservice.CategoryManager;
import com.iquest.service.clientservice.CategoryManagerImpl;
import com.iquest.service.exception.ServiceException;
import com.iquest.web.ParameterConstants;
import com.iquest.web.SessionConstants;
import com.iquest.web.exception.CriticalException;
import com.iquest.web.exception.InvalidUserInputException;

/**
 * Class used to fulfill requests associated with operations on
 * categories.
 * 
 * @author andrei.georgescu
 *
 */
public class CategoryController {
	/**
	 * Method used to persist a category.
	 * 
	 * @param request
	 *            The request object which is sent from the user.
	 * @param response
	 *            The response object which is going to be sent to the user.
	 * @return A string which represents the path to the next page displayed to
	 *         the user.
	 * @throws CriticalException
	 *             Thrown if there is a problem and the method can not fulfill
	 *             the user's request.
	 */
	public String fetchAddCategoryResponse(HttpServletRequest request,
			HttpServletResponse response) throws CriticalException {
		ResourceBundle languages = ResourceBundle.getBundle(
				ParameterConstants.INTERNATIONAL, request.getLocale());
		try {
			EntityManager entManager = (EntityManager) request
					.getAttribute(ParameterConstants.ENTITYMANAGER);

			CategoryManager categoryManager = new CategoryManagerImpl();
			Category category = extractCategory(request, entManager);

			categoryManager.addCategory(category, entManager);

			return "Controller?action=viewAccounts";
		} catch (InvalidUserInputException iuie) {
			try {
				setClientMessage(response, iuie.getMessage());
				return "UserPages/CategoryForm.jsp";
			} catch (IOException ioe) {
				throw new CriticalException(
						languages.getString("dispatcher.system.exception"));
			} catch (ServiceException se) {
				throw new CriticalException(
						languages.getString("dispatcher.system.exception"));
			}
		}
	}

	/**
	 * Method used to view the categories that an user possesses and return path
	 * to the next page the user will see.
	 * 
	 * @param request
	 *            The request object which is sent from the user.
	 * @param response
	 *            The response object which is going to be sent to the user.
	 * @return A string which represents the path to the next page displayed to
	 *         the user.
	 * @throws CriticalException
	 *             Thrown if there is a problem and the method can not fulfill
	 *             the user's request.
	 */
	public String fetchViewCategoriesResponse(HttpServletRequest request,
			HttpServletResponse response) throws CriticalException {
		ResourceBundle languages = ResourceBundle.getBundle(
				ParameterConstants.INTERNATIONAL, request.getLocale());
		try {
			HttpSession userSession = request.getSession(false);
			User user = (User) userSession.getAttribute(SessionConstants.USER);

			EntityManager entManager = (EntityManager) request
					.getAttribute(ParameterConstants.ENTITYMANAGER);

			CategoryManager categoryManager = new CategoryManagerImpl();
			List<Category> categories = categoryManager.getUserCategories(user,
					entManager);

			request.setAttribute("categories", categories);
			return "UserPages/TransactionForm.jsp";

		} catch (ServiceException se) {
			throw new CriticalException(
					languages.getString("dispatcher.system.exception"));
		}
	}

	/**
	 * Method used to extract a category from the user input and then return it.
	 * 
	 * @param request
	 *            The request sent from the user which contains the information
	 *            regarding the category.
	 * @param entManager
	 *            The entity manager used by the DAO classes to perform
	 *            operations .
	 * @return A category object.
	 * @throws InvalidUserInputException
	 *             Thrown if the input that is thrown from the user is invalid.
	 */
	private Category extractCategory(HttpServletRequest request,
			EntityManager entManager) throws InvalidUserInputException {
		Category category = new Category();

		String name = request.getParameter("name");
		String description = request.getParameter("description");
		String parentCategoryId = request.getParameter("parentCategory");

		if (name == null) {
			throw new InvalidUserInputException(
					"Category name can not be null!");
		}

		category.setName(name);
		category.setDescription(description);

		if (!parentCategoryId.equals("none")) {
			Long parentId = Long.parseLong(parentCategoryId);

			CategoryManager categoryManager = new CategoryManagerImpl();
			Category parent = categoryManager.getCategory(parentId, entManager);

			category.setParent(parent);

		}

		HttpSession userSession = request.getSession(false);
		User user = (User) userSession.getAttribute(SessionConstants.USER);
		category.setUser(user);

		return category;

	}

	/**
	 * Method used to set the message the user is going to see on the response.
	 * 
	 * @param response
	 *            The response object on which we are going to put the user
	 *            response message.
	 * @param message
	 *            The message to the user informing him if something has gone
	 *            sour.
	 * 
	 * @throws IOException
	 *             Thrown if we can not write the messages to the response.
	 *
	 */
	private void setClientMessage(HttpServletResponse response, String message)
			throws IOException {
		response.setContentType("text/html");
		PrintWriter responseMessage = response.getWriter();
		responseMessage.println(message);
	}

}
