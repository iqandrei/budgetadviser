package com.iquest.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ResourceBundle;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.iquest.persistence.constants.TransactionType;
import com.iquest.persistence.entity.Account;
import com.iquest.persistence.entity.Category;
import com.iquest.persistence.entity.Transaction;
import com.iquest.persistence.entity.User;
import com.iquest.service.account.AccountManagerImpl;
import com.iquest.service.clientservice.CategoryManager;
import com.iquest.service.clientservice.CategoryManagerImpl;
import com.iquest.service.exception.ServiceException;
import com.iquest.service.transactionservice.TransactionManager;
import com.iquest.service.transactionservice.TransactionManagerImpl;
import com.iquest.web.ParameterConstants;
import com.iquest.web.SessionConstants;
import com.iquest.web.exception.CriticalException;
import com.iquest.web.exception.InvalidUserInputException;

/**
 * Class used to fulfill requests associated with operations on categories.
 * 
 * @author andrei.georgescu
 *
 */
public class TransactionController {

	/**
	 * Method used to retrieve all the information that a transaction form
	 * requires and then return the path to the transaction form page that the
	 * user will see.
	 * 
	 * @param request
	 *            The request object which is sent from the user.
	 * @param response
	 *            The response object which is going to be sent to the user.
	 * @return A string which represents the path to the next page displayed to
	 *         the user.
	 * @throws CriticalException
	 *             Thrown if there is a problem and the method can not fulfill
	 *             the user's request.
	 */
	public String fetchTransactionFormResponse(HttpServletRequest request,
			HttpServletResponse response) throws CriticalException {
		ResourceBundle languages = ResourceBundle.getBundle(
				ParameterConstants.INTERNATIONAL, request.getLocale());

		try {
			HttpSession userSession = request.getSession(false);
			User user = (User) userSession.getAttribute(SessionConstants.USER);

			EntityManager entManager = (EntityManager) request
					.getAttribute(ParameterConstants.ENTITYMANAGER);

			CategoryManager categManager = new CategoryManagerImpl();
			AccountManagerImpl accountManager = new AccountManagerImpl();

			List<Category> categories = categManager.getUserCategories(user,
					entManager);
			List<Account> accounts = accountManager.retriveUserAccounts(user,
					entManager);

			request.setAttribute("categories", categories);
			request.setAttribute("accounts", accounts);

			String accountId = request.getParameter("accountId");
			request.setAttribute("accountId", accountId);

			return "UserPages/TransactionForm.jsp";

		} catch (InvalidUserInputException iuie) {
			try {
				setClientMessage(response, iuie.getMessage());
				return "UserPages/AccountsPage.jsp";

			} catch (IOException ie) {
				throw new CriticalException("dispatcher.system.exception");
			}
		} catch (ServiceException se) {
			throw new CriticalException(
					languages.getString("dispatcher.system.exception"));
		}
	}

	/**
	 * Method used to persist a transaction and then return the path to the next
	 * page that the user will see.
	 * 
	 * @param request
	 *            The request object which is sent from the user.
	 * @param response
	 *            The response object which is going to be sent to the user.
	 * @return A string which represents the path to the next page displayed to
	 *         the user.
	 * @throws CriticalException
	 *             Thrown if there is a problem and the method can not fulfill
	 *             the user's request.
	 */
	public String fetchAddTransactionResponse(HttpServletRequest request,
			HttpServletResponse response) throws CriticalException {
		ResourceBundle languages = ResourceBundle.getBundle(
				ParameterConstants.INTERNATIONAL, request.getLocale());
		try {
			EntityManager entManager = (EntityManager) request
					.getAttribute(ParameterConstants.ENTITYMANAGER);

			TransactionManager transManager = new TransactionManagerImpl();
			Transaction newTransaction = extractTransaction(request, entManager);
			transManager.addTransaction(newTransaction, entManager);

			System.out.println(request.getParameter("accountId"));

		} catch (InvalidUserInputException iuie) {
			try {
				setClientMessage(response, iuie.getMessage());
				return "UserPages/TransactionForm.jsp";

			} catch (IOException ie) {
				throw new CriticalException("dispatcher.system.exception");
			}
		} catch (ServiceException se) {
			throw new CriticalException(
					languages.getString("dispatcher.system.exception"));
		}
		return "Controller?action=VIEWACCOUNTINFO";
	}

	/**
	 * Method used to extract the transaction data from the request sent from
	 * the user.
	 * 
	 * @param request
	 *            The request object from which the request data is going to be
	 *            retrieved.
	 * @param entManager
	 *            The entity manager object used by the DAO classes to perform
	 *            operations.
	 * @return A transaction object with the information provided by the user
	 *         through the request.
	 */
	private Transaction extractTransaction(HttpServletRequest request,
			EntityManager entManager) {
		Transaction newTransaction = new Transaction();
		try {
			String note = request.getParameter("note");
			String amount = request.getParameter("amount");
			String date = request.getParameter("date");
			String type = request.getParameter("type");

			String account = request.getParameter("account");
			String category = request.getParameter("category");

			if (note == null) {
				throw new InvalidUserInputException(
						"Category name can not be empty!");
			}
			newTransaction.setNote(note);
			if (amount != null && !amount.isEmpty()) {
				newTransaction.setAmount(Double.parseDouble(amount));

			}

			newTransaction.setType(TransactionType.valueOf(type));

			SimpleDateFormat formatter = new SimpleDateFormat("dd/M/yyyy");
			newTransaction.setDate(formatter.parse(date));

			if (!category.equals("none")) {
				Long categoryId = Long.parseLong(category);

				CategoryManager categManager = new CategoryManagerImpl();

				newTransaction.setCategory(categManager.getCategory(categoryId,
						entManager));
			}
			Long accountId = Long.parseLong(account);

			AccountManagerImpl accountManager = new AccountManagerImpl();
			Account transactionAccount = accountManager.retrieveAccount(
					accountId, entManager);

			newTransaction.setSource(transactionAccount);

			return newTransaction;

		} catch (NumberFormatException nfe) {
			throw new InvalidUserInputException(
					"Starting amount needs to be a number", nfe);
		} catch (ParseException pe) {
			throw new InvalidUserInputException("Invalid Date format");
		}

	}

	/**
	 * Method used to set the message the user is going to see on the response.
	 * 
	 * @param response
	 *            The response object on which we are going to put the user
	 *            response message.
	 * @param message
	 *            The message to the user informing him if something has gone
	 *            sour.
	 * 
	 * @throws IOException
	 *             Thrown if we can not write the messages to the response.
	 *
	 */
	private void setClientMessage(HttpServletResponse response, String message)
			throws IOException {
		response.setContentType("text/html");
		PrintWriter responseMessage = response.getWriter();
		responseMessage.println(message);
	}

}
