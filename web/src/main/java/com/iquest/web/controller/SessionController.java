package com.iquest.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ResourceBundle;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iquest.persistence.entity.User;
import com.iquest.service.clientservice.UserManagerImpl;
import com.iquest.service.exception.ServiceException;
import com.iquest.web.ParameterConstants;
import com.iquest.web.exception.CriticalException;

/**
 * Class used to fulfill requests associated with operations on
 * user session.
 * 
 * @author DawnBreak
 *
 */
public class SessionController {
	private final Logger logger = LoggerFactory
			.getLogger(SessionController.class);

	/**
	 * Method used to login the user and to return the next page after login.
	 * 
	 * @param request
	 *            The request sent by the user to login.
	 * @param response
	 *            The response sent to the user containing information if the
	 *            login was unsuccessful.
	 * @return The next page that the user is going to see depending whether or
	 *         not the user successfully logs in.
	 * 
	 * @throws CriticalException
	 *             Thrown if the system is incapable to manage the request.
	 */
	public String fetchLoginDisplay(HttpServletRequest request,
			HttpServletResponse response) {
		ResourceBundle languages = ResourceBundle.getBundle(
				ParameterConstants.INTERNATIONAL, request.getLocale());

		try {
			UserManagerImpl userManager = new UserManagerImpl();
			String username = (String) request
					.getParameter(ParameterConstants.USERNAME);

			String password = (String) request
					.getParameter(ParameterConstants.PASSWORD);

			if (username.isEmpty() || password.isEmpty()) {
				setClientMessage(response,
						languages.getString("session.login.fail"));
				return "Login.jsp";
			}

			EntityManager entManager = (EntityManager) request
					.getAttribute(ParameterConstants.ENTITYMANAGER);

			boolean userExists = userManager.checkUserLoginInformation(
					username, password, entManager);

			if (userExists) {
				HttpSession session = request.getSession(true);
				User user = userManager.fetchUser(username, entManager);
				session.setAttribute("user", user);

				return "UserPages/UserPage.jsp";

			} else {
				setClientMessage(response,
						languages.getString("session.login.fail"));
				return "Login.jsp";
			}
		} catch (IOException ioe) {
			logger.error("Can not write to response writer", ioe);
			throw new CriticalException(
					languages.getString("dispatcher.system.exception"));
		} catch (ServiceException se) {
			logger.error("User can not be logged in", se);
			throw new CriticalException(
					languages.getString("dispatcher.system.exception"));
		}
	}

	/**
	 * Method used to logout the user and send the next page path for the user.
	 * 
	 * @param request
	 *            The request sent from the user to logout.
	 * @param response
	 *            The response sent from the server.
	 * @return The next page that the user should see.
	 * @throws CriticalException
	 *             Thrown if the system is incapable to manage the request.
	 */
	public String fetchLogoutDisplay(HttpServletRequest request,
			HttpServletResponse response) throws CriticalException {
		ResourceBundle languages = ResourceBundle.getBundle(
				ParameterConstants.INTERNATIONAL, request.getLocale());

		try {
			HttpSession userSession = request.getSession();
			userSession.invalidate();
			return "Login.jsp";
		} catch (ServiceException se) {
			logger.error("User can not be logged out");
			throw new CriticalException(
					languages.getString("dispatcher.system.exception"));
		}
	}

	/**
	 * Method used to set the message the user is going to see on the response.
	 * 
	 * @param response
	 *            The response object on which we are going to put the user
	 *            response message.
	 * @param message
	 *            The message to the user informing him if something has gone
	 *            sour.
	 * 
	 * @throws IOException
	 *             Thrown if we can not write the messages to the response.
	 *
	 */
	private void setClientMessage(HttpServletResponse response, String message)
			throws IOException {
		response.setContentType("text/html");
		PrintWriter responseMessage = response.getWriter();
		responseMessage.println(message);
	}
}
