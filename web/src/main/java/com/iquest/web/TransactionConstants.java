package com.iquest.web;

public abstract class TransactionConstants {
	public static final String NAME = "name";
	public static final String VALUE = "value";
	public static final String CURRENCY = "currency";
	public static final String OCCURENCY = "occurency";
	public static final String TYPE = "type";
	public static final String DATE = "date";

}
