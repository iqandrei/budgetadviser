package com.iquest.service.transactionservice;

import java.util.List;

import javax.persistence.EntityManager;

import com.iquest.persistence.dao.api.TransactionDao;
import com.iquest.persistence.dao.imlp.JpaTransactionDao;
import com.iquest.persistence.entity.Account;
import com.iquest.persistence.entity.Transaction;
import com.iquest.persistence.exception.DAOException;
import com.iquest.persistence.exception.DatabaseException;
import com.iquest.persistence.exception.EntityNotFoundException;
import com.iquest.service.exception.NoResultException;
import com.iquest.service.exception.ServiceException;

/**
 * * Business class used to operate on transactions.
 * 
 * @author DawnBreak
 *
 */
public class TransactionManagerImpl implements TransactionManager {

	@Override
	public void addTransaction(Transaction transaction, EntityManager entManager)
			throws ServiceException {
		try {
			TransactionDao transactionOperations = new JpaTransactionDao(
					entManager);
			transactionOperations.persist(transaction);

		} catch (DatabaseException de) {
			throw new ServiceException(
					"Transaction can not be persisted due to database problems",
					de);
		} catch (DAOException daoe) {
			throw new ServiceException(
					"Transaction can not be persisted due to dao problems",
					daoe);
		}

	}

	@Override
	public List<Transaction> getAccountTransactions(Account account,
			EntityManager entManager) throws NoResultException,
			ServiceException {
		try {

			TransactionDao transactionOperations = new JpaTransactionDao(
					entManager);
			return transactionOperations
					.findTransactionsTillTodayByAccount(account);

		} catch (EntityNotFoundException enfe) {
			throw new NoResultException(
					"No transactions associated with this account", enfe);

		} catch (DatabaseException de) {
			throw new ServiceException(
					"Could not retrieve account transactions due to database problems",
					de);
		} catch (DAOException daoe) {
			throw new ServiceException(
					"Could not retrive account transactions due to dao problems");
		}
	}

}
