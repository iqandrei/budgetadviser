package com.iquest.service.transactionservice;

import java.util.List;

import javax.persistence.EntityManager;

import com.iquest.persistence.entity.Account;
import com.iquest.persistence.entity.Transaction;
import com.iquest.service.exception.NoResultException;
import com.iquest.service.exception.ServiceException;

/**
 * Interface which contains the business logic which can be performed on
 * transactions.
 * 
 * @author andrei.georgescu
 *
 */
public interface TransactionManager {
	/**
	 * Method used to persist a transaction.
	 * 
	 * @param transaction
	 *            The transaction that we want to persist.
	 * 
	 * @param entManager
	 *            The entity manager object which is used for persistence
	 *            operations.
	 * @throws ServiceException
	 *             Thrown if there is a problem with adding the transaction.
	 */
	public void addTransaction(Transaction transaction, EntityManager entManager)
			throws ServiceException;

	/**
	 * Method used to retrieve the transactions of a particular user.
	 * 
	 * @param account
	 *            The account for which we want to retrieve the transactions.
	 * 
	 * @param entManager
	 *            The entity manager object which is used for persistence.
	 * 
	 * @return A list containing the transactions of a particular user.
	 * 
	 * @throws NoResultException
	 *             Thrown if there are no transactions associated with this
	 *             account.
	 * @throws ServiceException
	 *             Thrown if there is a problem in retrieving the transactions.
	 */
	public List<Transaction> getAccountTransactions(Account account,
			EntityManager entManager) throws NoResultException,
			ServiceException;
}
