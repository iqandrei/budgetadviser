package com.iquest.service.exception;

/**
 * Exception class that is the base class of all the business layer exceptions.
 * 
 * @author andrei.georgescu
 *
 */
public class ServiceException extends RuntimeException {
	/**
	 * Field used for class serialization.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor which calls the super class constructor with a throwable
	 * cause.
	 * 
	 * @param cause
	 *            The throwable which is going to be passed to the super class
	 *            constructor.
	 */
	public ServiceException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructor which calls the super class constructor with a message.
	 * 
	 * @param message
	 *            The string which is going to be passed to the super class
	 *            constructor.
	 */
	public ServiceException(String message) {
		super(message);
	}

	/**
	 * Constructor which calls the super class constructor with a message and a
	 * cause.
	 * 
	 * @param message
	 *            The string which is going to be passed to the super class
	 *            constructor.
	 * @param cause
	 *            The cause which is going to be passed to the super class
	 *            constructor.
	 */
	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
