package com.iquest.service.exception;

/**
 * Exception class which is thrown if a method that is supposed to retrieve an
 * item does not find the item.
 * 
 * @author andrei.georgescu
 *
 */
public class NoResultException extends ServiceException {
	/**
	 * Field used for class serialization.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor which calls the super class constructor with a throwable
	 * cause.
	 * 
	 * @param cause
	 *            The throwable which is going to be passed to the super class
	 *            constructor.
	 */
	public NoResultException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructor which calls the super class constructor with a message.
	 * 
	 * @param message
	 *            The string which is going to be passed to the super class
	 *            constructor.
	 */
	public NoResultException(String message) {
		super(message);
	}

	/**
	 * Constructor which calls the super class constructor with a message and a
	 * cause.
	 * 
	 * @param message
	 *            The string which is going to be passed to the super class
	 *            constructor.
	 * @param cause
	 *            The cause which is going to be passed to the super class
	 *            constructor.
	 */
	public NoResultException(String message, Throwable cause) {
		super(message, cause);
	}
}
