package com.iquest.service.clientservice;

import javax.persistence.EntityManager;

import com.iquest.persistence.entity.User;
import com.iquest.service.exception.ServiceException;

/**
 * Interface used as an api for the user manager service.
 * 
 * @author andrei.georgescu
 *
 */
public interface UserManager {

	/**
	 * Method used to add a user.
	 * 
	 * @param user
	 *            The user which is going to be added.
	 * @param entManager
	 *            The entity manager used by the DAO's to gain access to the
	 *            database.
	 * @throws ServiceException
	 *             when the system can not process the request.
	 */
	public void addUser(User user, EntityManager entManager)
			throws ServiceException;

	/**
	 * Method used to see if an user exists.
	 * 
	 * @param username
	 *            The username which corresponds to an user account.
	 * @param password
	 *            The password which corresponds to an user account.
	 * 
	 * @param entManager
	 *            The entity manager used by the DAO's to gain access to the
	 *            database.
	 * 
	 * @return A boolean which is true if the user login info exists and false
	 *         otherwise.
	 * @throws ServiceException
	 *             If the service can not perform the request.
	 * 
	 */
	public boolean checkUserLoginInformation(String username, String password,
			EntityManager entManager) throws ServiceException;

	/**
	 * Method used to see if an email already exists.
	 * 
	 * @param email
	 *            The email which corresponds to an user account.
	 * @param entManager
	 *            The entity manager used by the DAO classes for CRUD
	 *            operations.
	 * @return A boolean which is false if the email exists and true otherwise.
	 * @throws ServiceException
	 *             If the service can not perform the request.
	 */
	public boolean checkEmailAvailability(String email, EntityManager entManager)
			throws ServiceException;

	/**
	 * Method used to retrieve the user through his username.
	 * 
	 * @param username
	 *            The unique id which corresponds to an user.
	 * @param entManager
	 *            The entity manager used by the DAO classes for CRUD
	 *            operations.
	 * 
	 * @return The User with the specified username.
	 * @throws ServiceException
	 *             If the the service can not perform the request.
	 */
	public User fetchUser(String username, EntityManager entManager)
			throws ServiceException;
}
