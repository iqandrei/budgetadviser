package com.iquest.service.clientservice;

import java.util.List;

import javax.persistence.EntityManager;

import com.iquest.persistence.dao.api.CategoryDao;
import com.iquest.persistence.dao.api.UserDao;
import com.iquest.persistence.dao.imlp.JpaCategoryDao;
import com.iquest.persistence.dao.imlp.JpaUserDao;
import com.iquest.persistence.entity.Category;
import com.iquest.persistence.entity.User;
import com.iquest.persistence.exception.DatabaseException;
import com.iquest.persistence.exception.EntityNotFoundException;
import com.iquest.service.exception.NoResultException;
import com.iquest.service.exception.ServiceException;

/**
 * Business class used to operate on categories.
 * 
 * @author andrei.georgescu
 *
 */
public class CategoryManagerImpl implements CategoryManager {

	@Override
	public Category getCategory(long id, EntityManager entManager)
			throws ServiceException, NoResultException {
		try {

			CategoryDao categoryOperations = new JpaCategoryDao(entManager);
			return categoryOperations.findById(id);

		} catch (EntityNotFoundException enfe) {
			throw new NoResultException(
					"No category found associated with this id", enfe);
		} catch (DatabaseException de) {
			throw new ServiceException(
					"Can not retrieve the category due to database problems",
					de);
		}
	}

	@Override
	public void addCategory(Category category, EntityManager entManager)
			throws ServiceException {
		try {
			CategoryDao categoryOperations = new JpaCategoryDao(entManager);
			categoryOperations.persist(category);

		} catch (DatabaseException de) {
			throw new ServiceException(
					"Could not persist category due to database problems");
		}
	}

	@Override
	public List<Category> getUserCategories(User user, EntityManager entManager)
			throws ServiceException, NoResultException {
		try {
			UserDao userOperations = new JpaUserDao(entManager);

			User userData = userOperations.findById(user.getUsername());

			return (List<Category>) userData.getCategories();

		} catch (EntityNotFoundException enfe) {
			throw new NoResultException(
					"There are no categories associated with this account",
					enfe);
		} catch (DatabaseException de) {
			throw new ServiceException(
					"Could not retrive categories due to database problems");
		}
	}
}
