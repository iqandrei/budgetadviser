package com.iquest.service.clientservice;

import java.util.List;

import javax.persistence.EntityManager;

import com.iquest.persistence.dao.api.UserDao;
import com.iquest.persistence.dao.imlp.JpaUserDao;
import com.iquest.persistence.entity.Category;
import com.iquest.persistence.entity.User;
import com.iquest.persistence.exception.DAOException;
import com.iquest.persistence.exception.DatabaseException;
import com.iquest.persistence.exception.EntityNotFoundException;
import com.iquest.service.exception.NoResultException;
import com.iquest.service.exception.ServiceException;

/**
 * Business class used to operate on users.
 * 
 * @author andrei.georgescu
 *
 */
public class UserManagerImpl implements UserManager {

	@Override
	public void addUser(User user, EntityManager entManager)
			throws ServiceException {
		try {
			UserDao userOperations = new JpaUserDao(entManager);
			userOperations.persist(user);
		} catch (DatabaseException de) {
			throw new ServiceException("Database retrieval failure", de);
		} catch (DAOException de) {
			throw new ServiceException("System failure:DAO exception", de);
		}
	}

	@Override
	public boolean checkUserLoginInformation(String username, String password,
			EntityManager entManager) throws ServiceException {
		try {
			UserDao userOperations = new JpaUserDao(entManager);

			User user = userOperations.findById(username);

			String userPassword = user.getPassword();

			if (userPassword.equals(password)) {
				return true;
			}

			return false;

		} catch (EntityNotFoundException enfe) {
			return false;
		} catch (DatabaseException de) {
			throw new ServiceException("Database retrieval failure", de);
		} catch (DAOException de) {
			throw new ServiceException("System failure:DAO exception", de);
		}
	}

	@Override
	public boolean checkEmailAvailability(String email, EntityManager entManager)
			throws ServiceException {
		UserDao userOperations = new JpaUserDao(entManager);
		try {

			userOperations.findUserByEmail(email);
			return true;

		} catch (EntityNotFoundException enfe) {
			return false;
		} catch (DatabaseException de) {
			throw new ServiceException("Database retrieval failure", de);
		} catch (DAOException de) {
			throw new ServiceException(
					"The DAO layer has encountered a problem", de);
		}
	}

	@Override
	public User fetchUser(String username, EntityManager entManager)
			throws NoResultException, ServiceException {
		try {

			UserDao userOperations = new JpaUserDao(entManager);
			return userOperations.findById(username);

		} catch (EntityNotFoundException enfe) {
			throw new NoResultException(
					"There is no user associated with this username");
		} catch (DatabaseException de) {
			throw new ServiceException("Database retrieval failure", de);
		} catch (DAOException de) {
			throw new ServiceException(
					"The DAO layer has encountered a problem", de);
		}
	}
	
	public List<Category> getUserCategories(User user,
			EntityManager entManager) throws ServiceException,
			NoResultException {
		try {
			UserDao userOperations = new JpaUserDao(entManager);

			User userData =  userOperations.findById(user.getUsername());
			
			return (List<Category>) userData.getCategories();
			
		} catch (EntityNotFoundException enfe) {
			throw new NoResultException(
					"There are no categories associated with this account",
					enfe);
		} catch (DatabaseException de) {
			throw new ServiceException(
					"Could not retrive categories due to database problems");
		}
	}
	
	

}
