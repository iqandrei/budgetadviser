package com.iquest.service.clientservice;

import java.util.List;

import javax.persistence.EntityManager;

import com.iquest.persistence.entity.Category;
import com.iquest.persistence.entity.User;
import com.iquest.service.exception.NoResultException;
import com.iquest.service.exception.ServiceException;

/**
 * Interface which contains the business logic which can be performed on
 * categories.
 * 
 * @author andrei.georgescu
 *
 */
public interface CategoryManager {
	/**
	 * Method used to persist a category.
	 * 
	 * @param category
	 *            The category object which is going to be persisted.
	 * 
	 * @param entManager
	 *            The entity manager used by the DAO classes for CRUD
	 *            operations.
	 * 
	 * @throws ServiceException
	 *             Thrown if the category can not be added.
	 */
	public void addCategory(Category category, EntityManager entManager)
			throws ServiceException;

	/**
	 * Method used to retrieve the categories of the specified user.
	 * 
	 * @param user
	 *            The user for which the categories are being retrieved.
	 * @param entManager
	 *            The entity manager used by the DAO classes for CRUD
	 *            operations.
	 * 
	 * @return A list containing the categories which belong to the specified
	 *         user.
	 * @throws ServiceException
	 *             Thrown if the categories can not be retrieved.
	 * @throws NoResultException
	 *             Thrown if there are no categories associated with the given
	 *             user.
	 */
	public List<Category> getUserCategories(User user, EntityManager entManager)
			throws ServiceException, NoResultException;

	/**
	 * Method used to retrieve a category by its id.
	 * 
	 * @param id
	 *            The unique number associated with a category.
	 * 
	 * @param entManager
	 *            The entity manager used by the DAO classes for CRUD
	 *            operations.
	 * @return A category object which has the specified id.
	 * @throws ServiceException
	 *             Thrown if the category can not be retrieved.
	 * @throws NoResultException
	 *             Thrown if there is no category associated with this id.
	 * 
	 */
	public Category getCategory(long id, EntityManager entManager)
			throws ServiceException, NoResultException;
}
