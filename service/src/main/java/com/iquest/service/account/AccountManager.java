package com.iquest.service.account;

import java.util.List;

import javax.persistence.EntityManager;

import com.iquest.persistence.entity.Account;
import com.iquest.persistence.entity.Currency;
import com.iquest.persistence.entity.User;
import com.iquest.service.exception.NoResultException;
import com.iquest.service.exception.ServiceException;

/**
 * Interface which contains the business logic which can be performed on
 * accounts.
 * 
 * @author andrei.georgescu
 *
 */
public interface AccountManager {
	/**
	 * Method used to add a new account for an user.
	 * 
	 * @param newAccount
	 *            The account that is going to be added.
	 * @param entManager
	 *            The entity manager used by the DAO classes for CRUD
	 *            operations.
	 * 
	 * @throws IllegalArgumentException
	 *             If the account to be added is null.
	 * @throws ServiceException
	 *             Thrown if the account can not be persisted.
	 * 
	 */
	public void addUserAccount(Account newAccount, EntityManager entManager)
			throws IllegalArgumentException, ServiceException;

	/**
	 * Method used to retrieve the accounts of a particular user.
	 * 
	 * @param user
	 *            The user used for account retrieval.
	 * @param entManager
	 *            The entity manager used by the DAO classes.
	 * @return A list of accounts for a given user.
	 * 
	 * @throws ServiceException
	 *             Thrown if the accounts can not be retrieved.
	 * @throws NoResultException
	 *             Thrown if there are no accounts persisted that have the given
	 *             user.
	 */
	public List<Account> retriveUserAccounts(User user, EntityManager entManager)
			throws ServiceException, NoResultException;

	/**
	 * Method used to retrieve an account by his unique id.
	 * 
	 * @param accountId
	 *            The unique id associated with an account.
	 * @param entManager
	 *            The entity manager used by the DAO classes.
	 *            
	 * @return An account which object which contains the given id;
	 * 
	 * @throws ServiceException
	 *             Thrown if the account can not be retrieved.
	 * @throws NoResultException
	 *             Thrown if there is no account persisted that has the given
	 *             id.
	 */
	public Account retrieveAccount(Long accountId, EntityManager entManager)
			throws ServiceException, NoResultException;

	/**
	 * Method used to retrieve all the currencies that will be used for the
	 * creation of the account.
	 * 
	 * @param entManager
	 *            The entity manager used by the DAO classes.
	 * @return A list containing all the available currencies.
	 * 
	 * @throws NoResultException
	 *             Thrown if there are no currencies persisted.
	 * @throws ServiceException
	 *             Thrown if the currencies can not be retrieved.
	 */
	public List<Currency> retrieveAvailableCurrencies(EntityManager entManager)
			throws NoResultException, ServiceException;
}
