package com.iquest.service.account;

import java.util.List;

import javax.persistence.EntityManager;

import com.iquest.persistence.dao.api.AccountDao;
import com.iquest.persistence.dao.api.CurrencyDao;
import com.iquest.persistence.dao.imlp.JpaAccountDao;
import com.iquest.persistence.dao.imlp.JpaCurrencyDao;
import com.iquest.persistence.entity.Account;
import com.iquest.persistence.entity.Currency;
import com.iquest.persistence.entity.User;
import com.iquest.persistence.exception.DatabaseException;
import com.iquest.persistence.exception.EntityNotFoundException;
import com.iquest.service.exception.NoResultException;
import com.iquest.service.exception.ServiceException;

/**
 * Business class used to operate on account operations.
 * 
 * @author andrei.georgescu
 *
 */
public class AccountManagerImpl implements AccountManager {
	@Override
	public void addUserAccount(Account newAccount, EntityManager entManager)
			throws ServiceException {
		try {
			if (newAccount == null) {
				throw new IllegalArgumentException(
						"Account parameter can not be null!");
			}

			AccountDao account = new JpaAccountDao(entManager);
			account.persist(newAccount);
		} catch (DatabaseException de) {
			throw new ServiceException("Database can not persist account", de);
		}
	}

	@Override
	public List<Account> retriveUserAccounts(User user, EntityManager entManager)

	throws ServiceException, NoResultException {
		List<Account> userAccounts = null;
		try {
			if (user == null) {
				throw new IllegalArgumentException(
						"User parameter can not be null!");
			}

			AccountDao accountOperations = new JpaAccountDao(entManager);
			userAccounts = accountOperations.findUserAccounts(user);

		} catch (EntityNotFoundException nre) {
			throw new NoResultException(
					"Can not find any accounts associated with this user", nre);
		} catch (DatabaseException de) {
			throw new ServiceException(
					"Persistence error: can not retrieve accounts", de);
		}
		return userAccounts;

	}

	@Override
	public Account retrieveAccount(Long accountId, EntityManager entManager)
			throws ServiceException, NoResultException,
			IllegalArgumentException {
		try {
			if (accountId == null) {
				throw new IllegalArgumentException(
						"The accountId can not be empty!");
			}
			AccountDao accountOperations = new JpaAccountDao(entManager);
			return accountOperations.findById(accountId);

		} catch (EntityNotFoundException enfe) {
			throw new NoResultException(
					"Can not find the account associated with this id", enfe);
		} catch (DatabaseException de) {
			throw new ServiceException(
					"Persistence error: can not retrieve accounts", de);
		}
	}

	@Override
	public List<Currency> retrieveAvailableCurrencies(EntityManager entManager)
			throws NoResultException, ServiceException {
		try {

			CurrencyDao currencyOperations = new JpaCurrencyDao(entManager);
			return currencyOperations.findAll();

		} catch (EntityNotFoundException nsre) {
			throw new NoResultException("No currencies persisted", nsre);
		} catch (DatabaseException de) {
			throw new ServiceException(
					"Persistence error: can not retrieve currencies", de);
		}
	}
}
