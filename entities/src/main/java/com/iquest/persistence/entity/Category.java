package com.iquest.persistence.entity;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Class used to store the information of a category.
 * 
 * @author andrei.georgescu
 *
 */
@Entity
@Table(name = "category")
public class Category {
	/**
	 * The unique id associated with a category.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/**
	 * Used to specify the version of the persisted category.
	 */
	@Version
	private int version;

	/**
	 * The name of the category.
	 */
	@NotNull
	@Size(max = 50)
	private String name;

	/**
	 * The description of the category.
	 */
	@Column(nullable = true)
	private String description;

	/**
	 * The parents of the category.
	 */
	@ManyToOne
	private Category parent;

	/**
	 * The children of the category.
	 */
	@OneToMany
	private Collection<Category> children;

	/**
	 * The user which has the category.
	 */
	@ManyToOne
	@JoinColumn(name = "username")
	private User user;

	public long getId() {
		return id;
	}

	public int getVersion() {
		return version;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public Category getParent() {
		return parent;
	}

	public Collection<Category> getChildren() {
		return children;
	}

	public User getUser() {
		return user;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setParent(Category parent) {
		this.parent = parent;
	}

	public void setChildren(Collection<Category> children) {
		this.children = children;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
