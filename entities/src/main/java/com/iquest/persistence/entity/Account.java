package com.iquest.persistence.entity;

import java.math.BigDecimal;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

/**
 * Class used to store the information of an user account.
 * 
 * @author andrei.georgescu
 *
 */
@Entity
@Table(name = "account")
public class Account {
	/**
	 * The unique id associated with an user.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/**
	 * Used to specify the version of the persisted account.
	 */
	@Version
	private int version;

	/**
	 * The money that this account possesses.
	 */
	@Column(name = "balance", nullable = true)
	private BigDecimal balance;

	/**
	 * The title of this account.
	 */
	@Column(name = "title")
	@NotNull
	private String title;

	/**
	 * The currency  associated with this account.
	 */
	@ManyToOne
	@JoinColumn(name = "code")
	private Currency currencyCode;

	/**
	 * The user which possesses this account.
	 */
	@ManyToOne
	@JoinColumn(name = "username")
	@NotNull
	private User user;

	/**
	 * The list of transactions this account possesses.
	 */
	@OneToMany(mappedBy = "source")
	private Collection<Transaction> transactions;

	public long getId() {
		return id;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public String getTitle() {
		return title;
	}

	public Currency getCurrencyCode() {
		return currencyCode;
	}

	public User getUser() {
		return user;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setCurrencyCode(Currency currencyCode) {
		this.currencyCode = currencyCode;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Collection<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(Collection<Transaction> transactions) {
		this.transactions = transactions;
	}
}
