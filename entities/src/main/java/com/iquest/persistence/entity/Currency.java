package com.iquest.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class used to store a currency.
 * 
 * @author andrei.georgescu
 *
 */
@Entity
@Table(name = "currency")
public class Currency {

	/**
	 * Primary key of the entity which represents the currency code
	 */
	@Id
	private String code;

	/**
	 * The symbol associated with this particular currency.
	 */
	@Column(nullable = true)
	private String symbol;

	/**
	 * Small information regarding the currency such as the state or country it
	 * is used in.
	 */
	@Column(nullable = true)
	private String description;

	public String getCode() {
		return code;
	}

	public String getSymbol() {
		return symbol;
	}

	public String getDescription() {
		return description;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
