package com.iquest.persistence.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.iquest.persistence.constants.TransactionType;

/**
 * Class used to store the information of a client's transaction.
 * 
 * @author andrei.georgescu
 *
 */
@Entity
@Table(name = "transaction")
public class Transaction {

	/**
	 * The unique id associated with a transaction.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/**
	 * Used to specify the version of the persisted category.
	 */
	@Version
	private long version;

	/**
	 * The information used by the user to identify this particular transaction.
	 */
	@Column(name = "note", nullable = true)
	private String note;

	/**
	 * The sum of money associated with the transaction.
	 */
	@Column(name = "amount")
	@NotNull
	private double amount;

	/**
	 * The date on which this particular date was created.
	 */
	@Column(name = "date")
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	/**
	 * The type of the transaction, can be: Income, Expense or a Transaction
	 * between two accounts.
	 */
	@Enumerated(EnumType.STRING)
	@Column(name = "type")
	@NotNull
	private TransactionType type;

	/**
	 * The category which corresponds to this transaction.
	 */
	@ManyToOne
	@JoinColumn(name = "categoryId")
	private Category category;

	/**
	 * The exchange rate used when transferring from one account to another.
	 */
	@Column(name = "exchangeRate", nullable = true)
	private Float exchangeRate;

	/**
	 * The account which has the transaction.
	 */
	@ManyToOne
	@JoinColumn(name = "accountId")
	private Account source;

	/**
	 * The account which will be the destination of the transaction in a
	 * transfer between accounts.
	 */
	@ManyToOne
	private Account destination;

	public long getId() {
		return id;
	}

	public long getVersion() {
		return version;
	}

	public String getNote() {
		return note;
	}

	public double getAmount() {
		return amount;
	}

	public Date getDate() {
		return date;
	}

	public TransactionType getType() {
		return type;
	}

	public Category getCategory() {
		return category;
	}

	public Float getExchangeRate() {
		return exchangeRate;
	}

	public Account getSource() {
		return source;
	}

	public Account getDestination() {
		return destination;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setExchangeRate(Float exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public void setSource(Account source) {
		this.source = source;
	}

	public void setDestination(Account destination) {
		this.destination = destination;
	}

}
