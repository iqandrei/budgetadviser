package com.iquest.persistence.entity;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Class used to store the information of an User.
 * 
 * @author andrei.georgescu
 *
 */
@Entity
@Table(name = "user")
public class User {

	/**
	 * The username of the user which is uniquely associated with this account.
	 */
	@Id
	@NotNull
	@Size(max = 20)
	private String username;

	/**
	 * Used to specify the version of the persisted category.
	 */
	@Version
	private long version;

	/**
	 * The password of the user associated with this account.
	 */
	@Column(name = "password")
	@NotNull
	@Size(min = 6, max = 100)
	private String password;

	/**
	 * The email of the user's account.
	 */
	@Column(name = "email", unique = true)
	@NotNull
	private String email;

	/**
	 * The first name of the user.
	 */
	@Column(name = "firstname", nullable = true)
	private String firstname;

	/**
	 * The last name of the user.
	 */
	@Column(name = "lastname", nullable = true)
	private String lastname;

	/**
	 * The list of accounts that an user possesses.
	 */
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	private Collection<Account> accounts;

	/**
	 * The list of categoriess that an user possesses.
	 */
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	private Collection<Category> categories;

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public long getVersion() {
		return version;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public Collection<Account> getAccounts() {
		return accounts;
	}

	public Collection<Category> getCategories() {
		return categories;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public void setAccounts(Collection<Account> accounts) {
		this.accounts = accounts;
	}

	public void setCategories(Collection<Category> categories) {
		this.categories = categories;
	}

}
