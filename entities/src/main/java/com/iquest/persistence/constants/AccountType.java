package com.iquest.persistence.constants;

/**
 * Enum which holds the types of an account. It can be either a debit
 * account(which can not have the account balance negative) or credit(which can
 * have the account balance negative but only to a certain limit).
 * 
 * @author andrei.georgescu
 *
 */
public enum AccountType {
	DEBIT, CREDIT

}
