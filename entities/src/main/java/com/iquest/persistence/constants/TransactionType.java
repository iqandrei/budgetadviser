package com.iquest.persistence.constants;

/**
 * Enum which defines the types of a transaction. It can be either an income, an
 * expense or a transfer between two accounts.
 * 
 * @author andrei.georgescu
 *
 */
public enum TransactionType {
	INCOME, EXPENSE, TRANSFER
}
