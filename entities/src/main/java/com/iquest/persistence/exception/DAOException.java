package com.iquest.persistence.exception;

/**
 * Exception class that is the base class of all the dao layer exceptions.
 * 
 * @author andrei.georgescu
 *
 */
public class DAOException extends RuntimeException {

	/**
	 * Associated id user for serialization.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor which calls the super class constructor with a throwable
	 * cause.
	 * 
	 * @param cause
	 *            The throwable which is going to be passed to the super class
	 *            constructor.
	 */
	public DAOException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructor which calls the super class constructor with a message.
	 * 
	 * @param message
	 *            The string which is going to be passed to the super class
	 *            constructor.
	 */
	public DAOException(String message) {
		super(message);
	}

	/**
	 * Constructor which calls the super class constructor with a message and a
	 * cause.
	 * 
	 * @param message
	 *            The string which is going to be passed to the super class
	 *            constructor.
	 * @param cause
	 *            The cause which is going to be passed to the super class
	 *            constructor.
	 */
	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}
}
