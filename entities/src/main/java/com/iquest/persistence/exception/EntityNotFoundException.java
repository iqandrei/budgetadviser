package com.iquest.persistence.exception;

public class EntityNotFoundException extends DAOException {
	/**
	 * Associated id user for serialization.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor which calls the super class constructor with a throwable
	 * cause.
	 * 
	 * @param cause
	 *            The throwable which is going to be passed to the super class
	 *            constructor.
	 */
	public EntityNotFoundException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructor which calls the super class constructor with a message.
	 * 
	 * @param message
	 *            The string which is going to be passed to the super class
	 *            constructor.
	 */
	public EntityNotFoundException(String message) {
		super(message);
	}
	
	/**
	 * Constructor which calls the super class constructor with a message and a
	 * cause.
	 * 
	 * @param message
	 *            The string which is going to be passed to the super class
	 *            constructor.
	 * @param cause
	 *            The cause which is going to be passed to the super class
	 *            constructor.
	 */
	public EntityNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}
