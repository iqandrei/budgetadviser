package com.iquest.persistence.exception;

/**
 * Exception class that extends the DAOException and is thrown when there is a
 * problem with the database.
 * 
 * @author andrei.georgescu
 *
 */

public class DatabaseException extends DAOException {
	/**
	 * Associated id user for serialization.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor which calls the super class constructor with a throwable
	 * cause.
	 * 
	 * @param cause
	 *            The throwable which is going to be passed to the super class
	 *            constructor.
	 */
	public DatabaseException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructor which calls the super class constructor with a message and a
	 * cause.
	 * 
	 * @param message
	 *            The string which is going to be passed to the super class
	 *            constructor.
	 * @param cause
	 *            The cause which is going to be passed to the super class
	 *            constructor.
	 */
	public DatabaseException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * Constructor which calls the super class constructor with a message.
	 * 
	 * @param message
	 *            The string which is going to be passed to the super class
	 *            constructor.
	 */
	public DatabaseException(String message) {
		super(message);
	}

}
