package com.iquest.persistence.dao.api;

import com.iquest.persistence.entity.User;
import com.iquest.persistence.exception.DatabaseException;
import com.iquest.persistence.exception.EntityNotFoundException;

/**
 * Interface used to define the user dao api. It contains operations on
 * the user entity.
 * 
 * @author andrei.georgescu
 *
 */
public interface UserDao extends GenericDao<User> {
	/**
	 * Method used to find the user through the email associated with his
	 * account.
	 * 
	 * @param email
	 *            The email with which we search for the user.
	 * 
	 * @return The user which has the specified email.
	 * @throws EntityNotFoundException
	 *             If there is no user having such an email.
	 * @throws DatabaseException
	 *             If there is a problem with the database.
	 */
	public User findUserByEmail(String email) throws EntityNotFoundException,
			DatabaseException;
}
