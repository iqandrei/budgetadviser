package com.iquest.persistence.dao.imlp;

import java.sql.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.iquest.persistence.dao.api.AccountDao;
import com.iquest.persistence.entity.Account;
import com.iquest.persistence.entity.Transaction;
import com.iquest.persistence.entity.User;
import com.iquest.persistence.exception.DatabaseException;
import com.iquest.persistence.exception.EntityNotFoundException;

/**
 * DAO class used for querying the account table.
 * 
 * @author andrei.georgescu
 *
 */
public class JpaAccountDao extends GenericJpaDao<Account> implements AccountDao {

	/**
	 * Constructor used to initialize the entity manager field.
	 * 
	 * @param entManager
	 *            The entity manager object which is used in jpa operations.
	 */
	public JpaAccountDao(EntityManager entManager) {
		super(entManager);
	}

	@Override
	public List<Account> findUserAccounts(User user) throws DatabaseException,
			EntityNotFoundException {
		try {
			CriteriaQuery<Account> criteria = queryFactory
					.createQuery(Account.class);

			Root<Account> userEntity = criteria.from(Account.class);

			Expression<User> userAccount = userEntity.get("user");
			Predicate belongsToUser = queryFactory.equal(userAccount, user);

			criteria.where(belongsToUser);

			TypedQuery<Account> accountQuery = entManager.createQuery(criteria);
			List<Account> userAccounts = accountQuery.getResultList();

			if (userAccounts.isEmpty()) {
				throw new EntityNotFoundException("No accounts for this user");
			}

			return userAccounts;

		} catch (PersistenceException pe) {
			throw new DatabaseException("Database connectivity problem", pe);
		}
	}

	@Override
	public Account retrieveAccount(long accountId) throws DatabaseException,
			EntityNotFoundException {

		try {

			CriteriaQuery<Account> criteria = queryFactory
					.createQuery(Account.class);

			Root<Account> accountEntity = criteria.from(Account.class);

			Join<Account, Transaction> joinTransactionAccount = accountEntity
					.join("transactions");

			Expression<Date> transactionDate = joinTransactionAccount
					.get("date");

			Expression<Date> borderDate = queryFactory.currentDate();

			Predicate isRequiredAccount = queryFactory.equal(
					accountEntity.get("id"), accountId);

			Predicate untilDate = queryFactory.equal(transactionDate,
					borderDate);

			Predicate noTransactions = accountEntity.get("transactions")
					.isNull();

			criteria.where(queryFactory.and(isRequiredAccount,
					queryFactory.or(untilDate), noTransactions));

			TypedQuery<Account> transactionQuery = entManager
					.createQuery(criteria);

			return transactionQuery.getSingleResult();

		} catch (NoResultException nre) {
			throw new EntityNotFoundException("No accounts for this user", nre);
		} catch (PersistenceException pe) {
			throw new DatabaseException("Database connectivity problem", pe);
		}
	}
}
