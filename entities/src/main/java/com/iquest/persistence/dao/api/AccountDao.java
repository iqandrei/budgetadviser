package com.iquest.persistence.dao.api;

import java.util.List;

import com.iquest.persistence.entity.Account;
import com.iquest.persistence.entity.User;
import com.iquest.persistence.exception.DatabaseException;
import com.iquest.persistence.exception.EntityNotFoundException;

/**
 * Interface used to define the account dao api. It contains operations on the
 * account entity.
 * 
 * @author andrei.georgescu
 *
 */
public interface AccountDao extends GenericDao<Account> {
	/**
	 * Method used to retrieve all accounts of a given user.
	 * 
	 * @param user
	 *            The user for which the accounts are going to be retrieved.
	 *
	 * @return A list containing all the accounts of an user.
	 * 
	 * @throws DatabaseException
	 *             Thrown if there is a problem with the database.
	 * @throws EntityNotFoundException
	 *             Thrown if there are no accounts associated with this user.
	 */
	public List<Account> findUserAccounts(User user) throws DatabaseException,
			EntityNotFoundException;

	/**
	 * 
	 * @param accountId
	 *            The id used for account retrieval.
	 * @return The account which corresponds to the id given as a parameter.
	 * @throws DatabaseException
	 *             Thrown if the user can not be retrieved due to a persistence
	 *             problem.
	 * @throws EntityNotFoundException
	 *             Thrown if there is no user associated with the provided
	 *             account id.
	 */
	public Account retrieveAccount(long accountId) throws DatabaseException,
			EntityNotFoundException;
}
