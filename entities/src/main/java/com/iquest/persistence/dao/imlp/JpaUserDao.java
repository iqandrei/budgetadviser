package com.iquest.persistence.dao.imlp;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.iquest.persistence.dao.api.UserDao;
import com.iquest.persistence.entity.User;
import com.iquest.persistence.exception.DatabaseException;
import com.iquest.persistence.exception.EntityNotFoundException;

/**
 * DAO class used for querying the user table.
 * 
 * @author andrei.georgescu
 *
 */
public class JpaUserDao extends GenericJpaDao<User> implements UserDao {
	/**
	 * Constructor used to initialize the entity manager field.
	 * 
	 * @param entManager
	 *            The entity manager object which is used in jpa operations.
	 */
	public JpaUserDao(EntityManager entManager) {
		super(entManager);
	}

	@Override
	public User findUserByEmail(String email) throws EntityNotFoundException,
			DatabaseException {
		CriteriaQuery<User> criteria = queryFactory.createQuery(User.class);

		Root<User> userEntity = criteria.from(User.class);

		Expression<String> emailColumn = userEntity.get("email");
		Predicate emailExists = queryFactory.equal(emailColumn, email);

		criteria.where(emailExists);

		TypedQuery<User> emailQuery = entManager.createQuery(criteria);
		emailQuery.setMaxResults(1);

		try {
			return emailQuery.getSingleResult();

		} catch (NoResultException nre) {
			throw new EntityNotFoundException("Email is not in the database",
					nre);
		} catch (PersistenceException pe) {
			throw new DatabaseException("Database connectivity problem", pe);
		}
	}

}
