package com.iquest.persistence.dao.imlp;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.iquest.persistence.dao.api.TransactionDao;
import com.iquest.persistence.entity.Account;
import com.iquest.persistence.entity.Transaction;
import com.iquest.persistence.exception.DatabaseException;
import com.iquest.persistence.exception.EntityNotFoundException;

/**
 * DAO class used for querying the transaction table.
 * 
 * @author andrei.georgescu
 *
 */
public class JpaTransactionDao extends GenericJpaDao<Transaction> implements
		TransactionDao {

	/**
	 * Constructor used to initialize the entity manager field.
	 * 
	 * @param entManager
	 *            The entity manager object which is used in jpa operations.
	 */
	public JpaTransactionDao(EntityManager entManager) {
		super(entManager);
	}

	@Override
	public List<Transaction> findTransactionsTillTodayByAccount(Account account)
			throws DatabaseException, EntityNotFoundException {
		try {
			CriteriaQuery<Transaction> criteria = queryFactory
					.createQuery(Transaction.class);

			Root<Transaction> transactionColumn = criteria
					.from(Transaction.class);

			Expression<Account> transactionAccount = transactionColumn
					.get("source");

			Expression<Date> transactionDate = transactionColumn.get("date");

			Expression<Date> borderDate = queryFactory.currentDate();

			Predicate belongsToAccount = queryFactory.equal(transactionAccount,
					account);

			Predicate untilDate = queryFactory.lessThanOrEqualTo(
					transactionDate, borderDate);

			criteria.where(belongsToAccount, untilDate);
			criteria.orderBy(queryFactory.desc(transactionColumn.get("date")));

			TypedQuery<Transaction> transactionQuery = entManager
					.createQuery(criteria);

			List<Transaction> accountTransactions = transactionQuery
					.getResultList();

			if (accountTransactions.isEmpty()) {
				throw new EntityNotFoundException("No accounts for this user");
			}

			return accountTransactions;

		} catch (PersistenceException pe) {
			throw new DatabaseException("Database connectivity problem", pe);
		}
	}

	public BigDecimal retrieveTransactionSumForAccount()
			throws DatabaseException, EntityNotFoundException {

		try {
			CriteriaQuery<BigDecimal> criteria = queryFactory
					.createQuery(BigDecimal.class);

			Root<Transaction> transactionColumn = criteria
					.from(Transaction.class);

			Expression<BigDecimal> transactionAmount = transactionColumn
					.get("amount");

			Expression<Date> transactionDate = transactionColumn.get("date");

			criteria.select(queryFactory.sum(transactionAmount));

			Expression<Date> borderDate = queryFactory.currentDate();

			Predicate untilDate = queryFactory.lessThanOrEqualTo(
					transactionDate, borderDate);

			criteria.where(untilDate);
			System.out.println("cici");
			TypedQuery<BigDecimal> transactionQuery = entManager
					.createQuery(criteria);

			return transactionQuery.getSingleResult();

		} catch (NoResultException nre) {
			throw new EntityNotFoundException("No accounts for this user", nre);
		} catch (PersistenceException pe) {
			throw new DatabaseException("Database connectivity problem", pe);
		}
	}
}
