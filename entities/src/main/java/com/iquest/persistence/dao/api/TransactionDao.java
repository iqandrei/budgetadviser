package com.iquest.persistence.dao.api;

import java.util.List;

import com.iquest.persistence.entity.Account;
import com.iquest.persistence.entity.Transaction;
import com.iquest.persistence.exception.DAOException;
import com.iquest.persistence.exception.DatabaseException;
import com.iquest.persistence.exception.EntityNotFoundException;

/**
 * Interface used to define the transaction dao api. It contains operations on
 * the transaction entity.
 * 
 * @author andrei.georgescu
 *
 */
public interface TransactionDao extends GenericDao<Transaction> {

	/**
	 * Method used to find all user transactions from an account for today.
	 * 
	 * @param account
	 *            The account for which the transactions are going to be
	 *            retrieved.
	 * @return A list of transactions which belong to the given user.
	 * @throws DatabaseException
	 *             Thrown if the list of transactions can not be retrieved due
	 *             to database problems.
	 * @throws DAOException
	 *             Thrown if there is a problem of fetching the transactions due
	 *             to a dao layer problem.
	 * @throws EntityNotFoundException
	 *             Thrown if the account does not have transactions.
	 */
	public List<Transaction> findTransactionsTillTodayByAccount(Account account)
			throws DatabaseException, DAOException, EntityNotFoundException;

}
