package com.iquest.persistence.dao.imlp;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.iquest.persistence.dao.api.GenericDao;
import com.iquest.persistence.exception.DatabaseException;
import com.iquest.persistence.exception.EntityNotFoundException;

/**
 * Abstract class used to provide default implementation for CRUD operations.
 * 
 * @author andrei.georgescu
 *
 * @param <T>
 *            The entity with which the CRUD operations are going to use.
 */
public abstract class GenericJpaDao<T> implements GenericDao<T> {
	/**
	 * The entity manager object which is going to be used to connect to
	 * database.
	 */
	protected EntityManager entManager;
	/**
	 * The factory for queries.
	 */
	protected CriteriaBuilder queryFactory;

	/**
	 * The type of the template parameter.
	 */
	private final Class<T> type;

	/**
	 * Constructor used to initialize class fields.
	 * 
	 * @param entManager
	 */
	protected GenericJpaDao(EntityManager entManager) {
		type = (Class) getClassType();

		this.entManager = entManager;
		this.queryFactory = entManager.getCriteriaBuilder();
	}

	@Override
	public void persist(T entity) throws DatabaseException {
		try {
			EntityTransaction trans = entManager.getTransaction();
			trans.begin();

			entManager.persist(entity);

			trans.commit();
			entManager.close();
		} catch (PersistenceException pe) {
			throw new DatabaseException(pe);
		}
	}

	@Override
	public void remove(T entity) throws DatabaseException {
		try {
			EntityTransaction trans = entManager.getTransaction();
			trans.begin();

			entManager.remove(entity);

			trans.commit();
			entManager.close();
		} catch (PersistenceException pe) {
			throw new DatabaseException(pe);
		}
	}

	@Override
	public T findById(Object primaryKey) throws DatabaseException,
			EntityNotFoundException {
		try {
			EntityTransaction trans = entManager.getTransaction();
			trans.begin();

			T entity = entManager.find(type, primaryKey);
			trans.commit();

			if (entity == null) {
				throw new EntityNotFoundException(
						"The primary key is not in the database");
			}

			return entity;
		} catch (PersistenceException pe) {
			throw new DatabaseException(pe);
		}
	}

	@Override
	public void update(T entity) throws DatabaseException {
		try {

			EntityTransaction trans = entManager.getTransaction();
			trans.begin();

			this.entManager.merge(entity);
			trans.commit();
			entManager.close();
		} catch (PersistenceException pe) {
			throw new DatabaseException(pe);
		}
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List<T> findAll() throws DatabaseException, EntityNotFoundException {
		try {
			CriteriaQuery<T> criteria = queryFactory
					.createQuery(getClassType());

			Root<T> table = criteria.from(getClassType());

			criteria = criteria.select(table);

			TypedQuery<T> findAllQuery = entManager.createQuery(criteria);
			List<T> allColumns = findAllQuery.getResultList();

			if (allColumns.isEmpty()) {
				throw new EntityNotFoundException("No rows for this tables");
			}

			return allColumns;

		} catch (PersistenceException pe) {
			throw new DatabaseException("Database connectivity problem", pe);
		}

	}

	@SuppressWarnings("rawtypes")
	private Class getClassType() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		return (Class) pt.getActualTypeArguments()[0];

	}

}
