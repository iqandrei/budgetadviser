package com.iquest.persistence.dao.imlp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.iquest.persistence.dao.api.CategoryDao;
import com.iquest.persistence.entity.Category;
import com.iquest.persistence.entity.User;
import com.iquest.persistence.exception.DatabaseException;
import com.iquest.persistence.exception.EntityNotFoundException;

/**
 * DAO class used for querying the category table.
 * 
 * 
 * @author andrei.georgescu
 *
 */
public class JpaCategoryDao extends GenericJpaDao<Category> implements
		CategoryDao {

	/**
	 * Constructor used to initialize the entity manager field.
	 * 
	 * @param entManager
	 *            The entity manager object which is used in jpa operations.
	 */
	public JpaCategoryDao(EntityManager entManager) {
		super(entManager);
	}

	@Override
	public List<Category> findCategoriesByUser(User user)
			throws DatabaseException, EntityNotFoundException {

		try {
			CriteriaQuery<Category> criteria = queryFactory
					.createQuery(Category.class);

			Root<User> userEntity = criteria.from(User.class);

			Expression<User> userColumn = userEntity.get("user");
			Predicate belongsToUser = queryFactory.equal(userColumn, user);

			criteria.where(belongsToUser);

			TypedQuery<Category> accountQuery = entManager
					.createQuery(criteria);
			List<Category> userCategories = accountQuery.getResultList();

			if (userCategories.isEmpty()) {
				throw new EntityNotFoundException("No categories for this user");
			}

			return userCategories;

		} catch (PersistenceException pe) {
			throw new DatabaseException("Database connectivity problem", pe);
		}
	}

}
