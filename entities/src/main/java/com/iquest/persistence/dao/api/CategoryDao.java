package com.iquest.persistence.dao.api;

import java.util.List;

import com.iquest.persistence.entity.Category;
import com.iquest.persistence.entity.User;
import com.iquest.persistence.exception.DatabaseException;
import com.iquest.persistence.exception.EntityNotFoundException;

/**
 * Interface used to define the category dao api. It contains operations on the
 * category entity.
 * 
 * @author andrei.georgescu
 *
 */
public interface CategoryDao extends GenericDao<Category> {
	/**
	 * Method used to find all the categories of a given user.
	 * 
	 * @param user
	 *            The user for which the categories are retrieved.
	 * @return A list containing the categories which correspond to the given
	 *         user.
	 * @throws DatabaseException
	 *             Thrown if there is a database problem when finding the
	 *             categories.
	 * @throws EntityNotFoundException
	 *             Thrown if there are no results returned from the query.
	 */
	public List<Category> findCategoriesByUser(User user)
			throws DatabaseException, EntityNotFoundException;
}
