package com.iquest.persistence.dao.api;

import com.iquest.persistence.entity.Currency;

/**
 * Interface used to define the currency dao api. It contains operations on the
 * currency entity.
 * 
 * @author andrei.georgescu
 *
 */
public interface CurrencyDao extends GenericDao<Currency> {

}
