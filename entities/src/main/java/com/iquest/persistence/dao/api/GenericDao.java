package com.iquest.persistence.dao.api;

import java.util.List;

import com.iquest.persistence.exception.DatabaseException;
import com.iquest.persistence.exception.EntityNotFoundException;

/**
 * The interface which is going to be extended by all dao api's. It contains the
 * CRUD operations on the database.
 * 
 * @author andrei.georgescu
 *
 * @param <T>
 *            The entity which is going to be used in the CRUD operations.
 */
public interface GenericDao<T> {
	/**
	 * Method used to save an entity object into the database.
	 * 
	 * @param entity
	 *            The entity which is going to be persisted.
	 * 
	 * @throws DatabaseException
	 *             Thrown if the database operation has not completed
	 *             successfully.
	 */
	void persist(T entity) throws DatabaseException;

	/**
	 * Method used to remove an entity from the database.
	 * 
	 * @param entity
	 *            The entity which is going to be removed.
	 * @throws DatabaseException
	 *             Thrown if the database operation has not completed
	 *             successfully.
	 */
	void remove(T entity) throws DatabaseException;

	/**
	 * Method used to find an entity by his primary key.
	 * 
	 * @param id
	 *            The primary key of the user.
	 * @return An entity which has the id as primary key.
	 * @throws DatabaseException
	 *             Thrown if the database operation has not completed
	 *             successfully.
	 * @throws EntityNotFoundException
	 *             Thrown if there is no entity in the database associated with
	 *             the given primary key.
	 */
	T findById(Object id) throws DatabaseException, EntityNotFoundException;

	/**
	 * Method used to update the information of an entity.
	 * 
	 * @param entity
	 *            The new entity which is going to update the old entity record.
	 * @throws DatabaseException
	 *             Thrown if the database operation has not completed
	 *             successfully.
	 */
	void update(T entity) throws DatabaseException;

	/**
	 * Method used to find all the entities.
	 * 
	 * @return A list containing all the currencies in the database.
	 * 
	 * @throws DatabaseException
	 *             Thrown if the database operation has not completed
	 *             successfully.
	 * @throws EntityNotFoundException
	 *             Thrown if the there are no values for the specified entity.
	 */
	List<T> findAll() throws DatabaseException, EntityNotFoundException;
}
