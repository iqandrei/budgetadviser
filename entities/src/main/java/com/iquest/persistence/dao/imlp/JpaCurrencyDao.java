package com.iquest.persistence.dao.imlp;

import javax.persistence.EntityManager;

import com.iquest.persistence.dao.api.CurrencyDao;
import com.iquest.persistence.entity.Currency;

/**
 * DAO class used for querying the currency table.
 *
 * @author andrei.georgescu
 *
 */
public class JpaCurrencyDao extends GenericJpaDao<Currency> implements
		CurrencyDao {
	/**
	 * Constructor used to initialize the entity manager field.
	 * 
	 * @param entManager
	 *            The entity manager object which is used in jpa operations.
	 */
	public JpaCurrencyDao(EntityManager entManager) {
		super(entManager);
	}

}
